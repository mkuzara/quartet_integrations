# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2020 SerialLab Corp.  All rights reserved.
from logging import getLogger, getLevelName, DEBUG

from io import BytesIO
from lxml import etree

from django.conf import settings
from django.template import loader
from rest_framework import status
from rest_framework.response import Response
from quartet_capture.models import TaskParameter
from serialbox.api.views import AllocateView
from quartet_integrations.systech.guardian.views import GuardianNumberRangeRenderer
logger = getLogger(__name__)


class SAPNumberRangeView(AllocateView):
    """
    Will process inbound Guardian Number Range requests and return accordingly.
    This is a SOAP interface and supports only the POST operation.
    """
    renderer_classes = [GuardianNumberRangeRenderer]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.type = None
        self.machine_name = None
        self.ext_digit = None
        self.id_type = None
        self.sender_gln = None
        self.receiver_gln = None

    def get(self, request, pool=None, size=None, region=None):
        """
        Just serves up the tracelink WSDL file for optel connectors and
        applications that require the WSDL to function.
        """
        host = request._request.get_host()
        scheme = request._request.scheme
        if 'wsdl' in request.query_params.keys():
            template = loader.get_template(
                "tracelink/snrequest.xml")
            xml = template.render({"host": host,
                                   "scheme": scheme
                                   })
            return Response(xml, status.HTTP_200_OK,
                            content_type='application/xml')
        else:
            return Response(status=status.HTTP_200_OK)

    def post(self, request):
        request_data = \
            etree.iterparse(BytesIO(request.body),
                            events=('end',),
                            remove_comments=True)
        count = None
        count = self.parse_xml(request_data)
        if self.ext_digit:
            machine_name = self.ext_digit + self.machine_name
        else:
            machine_name = self.machine_name
        ret = super().get(request, machine_name, count)
        return ret

    def parse_xml(self, data):
        count = 0
        for event, element in data:
            print(element.tag)
            if 'ObjectKey' in element.tag:
                self.check_object_key(element)
            elif 'Size' in element.tag:
                count = element.text
            elif 'IDType' in element.tag:
                self.id_type = element.text
            print(element.tag)
        return count

    def check_object_key(self, object_key: etree.Element) -> tuple:
        """
        Iterates through the children of an ObjectKey element to ascertain
        whether or not there is a GTIN or SSCC identifier present.
        :param element: The element to check
        :return: Will return the GTIN or SSCC if found, otherwise none.
        """
        pool_type_found = False
        sender_gln = False
        receiver_gln = False
        ext_digit = False
        for child in object_key:
            if child.text and (
                'GTIN' in child.text
                or 'COMPANY_PREFIX' in child.text
            ):
                pool_type_found = True
                self.type = child.text
            elif pool_type_found and 'Value' in child.tag:
                self.machine_name = child.text
            elif child.text and (
                'EXT_DIGIT' in child.text
            ):
                ext_digit=True
            elif ext_digit and 'Value' in child.tag:
                self.ext_digit = child.text
            elif child.text and (
                'SENDER_GLN' in child.text
            ):
                sender_gln = True
            elif sender_gln and 'Value' in child.tag:
                self.sender_gln = child.text
            elif child.text and (
                'RECEIVER_GLN' in child.text
            ):
                receiver_gln = True
            elif receiver_gln and 'Value' in child.tag:
                self.receiver_gln = child.text
        return

    def _set_task_parameters(self, pool, region, response_rule, size, request):
        """
        Override the _set_task_parameters so that we can pass in the
        additional systech parameters for the rule.
        """
        db_task = super()._set_task_parameters(pool, region, response_rule,
                                               size,
                                               request)
        TaskParameter.objects.create(
            task=db_task,
            name='id_type',
            value=self.id_type
        )
        TaskParameter.objects.create(
            task=db_task,
            name='type',
            value=self.type
        )
        TaskParameter.objects.create(
            task=db_task,
            name='machine_name',
            value=self.machine_name
        )
        TaskParameter.objects.create(
            task=db_task,
            name='sender_gln',
            value=self.sender_gln
        )
        TaskParameter.objects.create(
            task=db_task,
            name='receiver_gln',
            value=self.receiver_gln
        )
        if self.ext_digit:
            TaskParameter.objects.create(
                task=db_task,
                name='ext_digit',
                value=self.ext_digit
            )
        return db_task
