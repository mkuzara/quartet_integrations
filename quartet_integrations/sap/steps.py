# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2019 SerialLab Corp.  All rights reserved.
import io
from lxml import etree
from quartet_capture import models
from quartet_epcis.parsing.steps import ContextKeys
from django.core.files.base import File
from quartet_capture.rules import Step, RuleContext
from quartet_integrations.sap.parsing import SAPParser
from quartet_integrations.serialbox.steps import ContextKeys as SerialboxContextKeys
from quartet_output.steps import ContextKeys as OutputContextKeys


class SAPParsingStep(Step):
    """
    A QU4RTET parsing step that can parse SAP XML data that contains
    custom event data.
    """

    def execute(self, data, rule_context: RuleContext):
        data = self.get_data(data)
        # the base class will return a generic message id for the
        # parsed epcis data
        self.info('Beginning parsing...')
        message_id = self._parse(data)
        self.info('Adding Message ID %s to the context under '
                  'key MESSAGE_ID.', message_id)
        self.info('Parsing complete.')
        # add the message id to the context in case rules downstream
        # need access to the data placed into the database as part
        # of this processing
        rule_context.context[
            ContextKeys.EPCIS_MESSAGE_ID_KEY.value
        ] = message_id

    def _parse(self, data):
        return SAPParser(data).parse()

    def get_data(self, data):
        try:
            if not isinstance(data, File):
                data = io.BytesIO(data)
        except TypeError:
            try:
                data = io.BytesIO(data.encode())
            except AttributeError:
                self.error('Could not convert the inbound data into an '
                           'expected format for the parser.')
                raise
        return data

    def on_failure(self):
        pass

    @property
    def declared_parameters(self):
        pass


class SAPAsyncNumberRequestStep(Step):

    def __init__(self, db_task, **kwargs):
        super().__init__(db_task, **kwargs)
        self.receiver_gln = None
        self.sender_gln = None
        self.ext_digit = None
        self.machine_name = None
        self.type = None
        self.msg_id = None
        self.rel_msg_id = None
        self.file_name = None
    
    def execute(self, data, rule_context: RuleContext):
        super().execute(data, rule_context)
        request_data = \
            etree.iterparse(io.BytesIO(data),
                            events=('end',),
                            remove_comments=True)
        count = None
        self.info('Parsing inbound data...')
        count = self.parse_xml(request_data)
        if self.ext_digit:
            machine_name = self.ext_digit + self.machine_name
        else:
            machine_name = self.machine_name
        self.info('Machine name %s for serial number pool detected' % self.machine_name)
        rule_context.context[
            SerialboxContextKeys.MACHINE_NAME_KEY.value
        ] = machine_name
        rule_context.context[
            SerialboxContextKeys.ALLOCATION_COUNT_KEY.value
        ] = count

        rule_context.context['MessageId'] = self.msg_id

    def parse_xml(self, data):
        count = 0
        for event, element in data:
            print(element.tag)
            if 'ObjectKey' in element.tag:
                self.check_object_key(element)
            elif 'Size' in element.tag:
                count = element.text
            elif 'IDType' in element.tag:
                self.id_type = element.text
            elif 'MessageId' == element.tag:
                self.msg_id = element.text
            print(element.tag)
        return count

    def check_object_key(self, object_key: etree.Element) -> tuple:
        """
        Iterates through the children of an ObjectKey element to ascertain
        whether or not there is a GTIN or SSCC identifier present.
        :param element: The element to check
        :return: Will return the GTIN or SSCC if found, otherwise none.
        """
        pool_type_found = False
        sender_gln = False
        receiver_gln = False
        ext_digit = False
        for child in object_key:
            if child.text and (
                'GTIN' in child.text
                or 'COMPANY_PREFIX' in child.text
                or 'SSCC' in child.text
            ):
                pool_type_found = True
                self.type = child.text
            elif pool_type_found and 'Value' in child.tag:
                self.machine_name = child.text
            elif child.text and (
                'EXT_DIGIT' in child.text
            ):
                ext_digit=True
            elif ext_digit and 'Value' in child.tag:
                self.ext_digit = child.text
            elif child.text and (
                'SENDER_GLN' in child.text
            ):
                sender_gln = True
            elif sender_gln and 'Value' in child.tag:
                self.sender_gln = child.text
            elif child.text and (
                'RECEIVER_GLN' in child.text
            ):
                receiver_gln = True
            elif receiver_gln and 'Value' in child.tag:
                self.receiver_gln = child.text
            
    def on_failure(self):
        return super().on_failure()
    
    def declared_parameters(self):
        return super().declared_parameters


class SAPAddAdditionalInfoToSNResponseStep(Step):

    def __init__(self, db_task, **kwargs):
        super().__init__(db_task, **kwargs)
    
    def execute(self, data, rule_context: RuleContext):
        super().execute(data, rule_context)
        msg_id = rule_context.context.get('MessageId')
        # get repsonse data
        outbound_msg = rule_context.context.get(
            OutputContextKeys.OUTBOUND_EPCIS_MESSAGE_KEY.value)
        # format response data
        outbound_msg = outbound_msg.format(message_id=msg_id)
        # add response data to outbound key
        rule_context.context[
            OutputContextKeys.OUTBOUND_EPCIS_MESSAGE_KEY.value
        ] = outbound_msg
    
    def on_failure(self):
        return super().on_failure()
    
    def declared_parameters(self):
        return super().declared_parameters