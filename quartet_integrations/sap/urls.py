from django.conf.urls import url
from quartet_integrations.sap.views import SAPNumberRangeView

urlpatterns = [
    url(
        r'SAP/NumberRangeService/?',
        SAPNumberRangeView.as_view(), name="SAPNumberRangeService"
    ),
]
